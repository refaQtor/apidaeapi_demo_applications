You may begin by viewing the well [documented header file](https://bitbucket.org/buildlab/apidaeapi_demo_applications/src/HEAD/Qt/ApidaeAPI/apidaeapiwindow.h?at=master&fileviewer=file-view-default) in the source:  ApidaeAPI_demo_applications / Qt / ApidaeAPI / apidaeapiwindow.h 

You may also download and use it according to the terms of the MPLv2 license.

Or, simply contact us for direct assistance at [Apidaelabs](http://apidaelabs.com/)