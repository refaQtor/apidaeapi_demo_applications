/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
#include "apidaeapiwindow.h"
#include <QApplication>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("BUILDlab");
    QCoreApplication::setOrganizationDomain("Apidaelabs.com");
    QCoreApplication::setApplicationName("ApidaeAccelerator");
    QString idf = (argc > 0) ? QString(argv[1]) : "";
    QString epw = (argc > 1) ? argv[2] : "";
    ApidaeAPIexample w(idf, epw);
    w.show();

    return a.exec();
}
