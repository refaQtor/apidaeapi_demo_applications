#-------------------------------------------------
#
# Project created by QtCreator 2015-03-11T10:33:09
#
#-------------------------------------------------

QT       += core gui network websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ApidaeAPI
TEMPLATE = app


SOURCES += main.cpp\
        apidaeapiwindow.cpp

HEADERS  += apidaeapiwindow.h

FORMS    += apidaeapiwindow.ui
