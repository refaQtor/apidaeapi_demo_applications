/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
#ifndef APIDAEAPIWINDOW_H
#define APIDAEAPIWINDOW_H

#include <QFile>
#include <QMainWindow>
#include <QWebSocket>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QHttpMultiPart>
#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include <QtNetwork/QSslError>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QUrl>

namespace Ui {
class ApidaeAPIwindow;
}

/**
 * @page APIDAE_REST
 * @brief APIDAE_REST
 *
 * The RESTful endpoint for Apidae API is at this url:
 *
 *      "https://api.apidaelabs.com/accelerator/model/run.json"
 *
 */
static const QString APIDAE_REST("https://apidaelabs.com/accelerator/model/run.json");
/**
 * @page APIDAE_SOCKET
 * @brief APIDAE_SOCKET
 *
 * The Apidae API responds to websocket activity at this url:
 *
 *      "wss://apidaelabs.com:8484"
 *
 */
static const QString APIDAE_SOCKET("wss://apidaelabs.com:8484");
/**
 * @page APIDAE_TOKEN
 * @brief APIDAE_TOKEN
 *
 * You will be required to provide an authorization token when submitting a job.
 * API keys are
 *
 * go to http://apidaelabs.com
 * and sign up for a free trial.  An API key will be emailed to you.
 * It is the 256 character long string after the "token=" in the email.
 *
 */


class ApidaeAPIexample : public QMainWindow
{
    Q_OBJECT

public slots:
    /**
     * @brief ApidaeAPIexample::on_action_Simulate_triggered
     *
     * This C++ example is using the network facilities of Qt5.
     * Any capable language and framework will have similar abilities.
     *
     * The important part is the order and form of the messages
     * to be transferred over the RESTful and websocket connections.
     * The methods/activities are described in roughly the order
     * they'll need to be executed.
     *
     */
    void on_action_Simulate_triggered();

    /**
     * @brief ApidaeAPIexample::socketSetupConnection
     *
     * Initial activity to establish connection with the Apidae API.
     *
     * Set up signal/socket connections, or perhaps callbacks,
     * to the appropriate methods to respond to messages on the websocket,
     * and "open" a websocket connection at the correct @ref APIDAE_SOCKET
     *
     */
    void socketSetupConnection(const QUrl &url);

    /**
     * @brief onSocketConnected
     *
     * Once the websocket connection is established, you need to request
     * an id for this particular websocket connection.  Do this by sending:
     *
     *      {"event": "/uuid"}
     *
     * The response will come back as a json formatted object:
     *
     *      {"ws_uuid": "this-websocket-instance-uuid"}
     *
     * which you will catch in the onSocketTextReceived() method;
     *
     * Keep track of this uuid result, as it may be used in conjunction
     * with multiple POSTs to the RESTful interface.  This uuid associates the
     * events involving the POST to this websocket, so that you can listen
     * here for activity.
     *
     */
    void onSocketConnected();

    /**
     * @brief onSocketTextReceived
     *
     * Assuming that you've made the suitable signal/slot or callback arrangements,
     * all activity returns on the single websocket, and this method listens
     * to all of it.  You'll need a switch on these messages to filter and respond
     * differently as appropriate.
     *
     * The first thing to arrive, in response to your request in onSocketConnected(),
     * is the websocket identifier.  With these items :
     *
     * - email address,
     * - IDF file data,
     * - EPW file data,
     * - API token,
     * - websocket uuid
     *
     * you have what you need to POST to the RESTful endpoint to
     * execute an energy simulation.  So, it is right in this switch's case that
     * we assemble the items and make the POST.  Essentially, you need to populate
     * a multipart message with the appropriate data, and POST it to @ref APIDAE_REST
     * When the simulation actually starts, you'll get a message like this:
     *
     *      {"event": "/study/status",
     *        "type": "fast",
     *        "model_space_uuid": "ff30fbaf-fb7f-480f-8e9d-cde86632af73",
     *        "study_uuid": "f6a639bd-ea50-4d55-83f6-e304b7fb6c25",
     *        "start": 1431635292623,
     *        "data": "{\n  \"data\": {\n    \"uuid\": \"fd312c46-97ea-483e-84c0-839e41a6a2e2\",
     *                  \n \"$duration\": 33946,\n \"$response\": {\n \"error\": null\n }\n }\n}"}
     *
     * At the completion of each simulation, you'll receive a message
     * with "results_url" as one of the keys, and the "event" will be "/study/complete".
     *
     *      {"type": "fast",
     *        "study_uuid": "f6a639bd-ea50-4d55-83f6-e304b7fb6c25",
     *        "model_space_uuid": "ff30fbaf-fb7f-480f-8e9d-cde86632af73",
     *        "results_url": "https://apidae.s3.amazonaws.com/model_space_data/main
     *                          /ff30fbaf-fb7f-480f-8e9d-cde86632af73
     *                          /e4476d4c-257b-4966-a34c-f1b125b1160f/results_fast.zip",
     *        "start": 1431635292623,
     *        "end": 1431635327981,
     *        "elapsed_time": 35358,
     *        "event": "/study/complete",
     *        "status": { "run_state": "valid",
     *        "run_state_message": "[LOG]sim returned with valid state:[/LOG]None" }}
     *
     * You'll want to check that "status": { "run_state": ... ) is "valid".  If so,
     * we extract the URL value from that key, and initiate a download of that .ZIP file.
     *
     * If  "status": { "run_state": ... ) is "failed", then you'll wan't to provide the
     * "run_state_message" to the user to interpret the EnergyPlus error file contents.
     * It will look something like this:
     *
     *      "[LOG]sim returned with failed state:[/LOG]Program Version,
     *      EnergyPlus-Linux-OMP-64 8.0.0.008, YMD=2015.05.14 21:18,IDD_Version 8.0.0.008|
     *      .... the complete error messages of energyplus.err file ....
     *      EnergyPlus Terminated--Fatal Error Detected. 3 Warning; 3 Severe Errors;
     *      Elapsed Time=00hr 00min  1.94sec"}}"
     *
     *
     * In each case, you may also wish to connect any progress signals to handlers to display
     * progress of upload or download.  At the moment, actual simulation progress status
     * is not reflected here, though it is in the roadmap to provide that.
     *
     */
    void onSocketTextReceived(QString message);

    /**
     * @brief restReadyRead
     *
     * You may wish to collect the information in response to the POST itself. If so,
     * setup signal/slot connection or callback to catch this response on the RESTful
     * interface (not this websocket); it will return uuid identifiers for both the FAST
     * and STANDARD simulations that you can use to re-associate the results to the inputs.
     *
     *      {"data": {"model_space":"ff30fbaf-fb7f-480f-8e9d-cde86632af73",
     *      "slow_study":"0309d4c4-c629-459c-88e9-c9cca93c42ae",
     *      "fast_study":"f6a639bd-ea50-4d55-83f6-e304b7fb6c25"}}
     *
     * This is especially useful when POSTing very many simulation jobs for either
     * queued or parallel execution.
     *
     */
    void restReadyRead();

    void downloadFinished(QNetworkReply *reply);

    void updateProgress(qint64 current, qint64 total);

    void on_action_IDF_triggered();

    void on_action_EPW_triggered();

    void on_action_Reference_triggered();

    void on_action_Quit_triggered();


public:
    explicit ApidaeAPIexample(QString idf_file, QString epw_file, QWidget *parent = 0);
    ~ApidaeAPIexample();

Q_SIGNALS:
    void closed();

private:
    Ui::ApidaeAPIwindow *ui;

    QWebSocket m_webSocket;
    QNetworkAccessManager manager;
    QNetworkRequest *m_request;
    QHttpMultiPart *m_files_post;
    QNetworkReply *download_reply;
    QNetworkReply *rest_reply;

    QString m_idf_file;
    QString m_epw_file;
    QString m_accelerator_job_uuid;
    QString m_apidae_api_token;
    QString m_email;
    QString m_output_filepath;
    QString m_rest_url;
    QString m_websocket_url;
    bool m_standard;
    bool m_accelerated;
    bool m_ready_for_simulation;

    bool saveToDisk(const QString &filename, QIODevice *data);

    void loadSettings();
    void saveSettings();
    void enableSimulationIfReady();
    void disableInput();
    void disableSimulation();
    void enableSimulation();

private slots:
    void on_lineEdit_email_textChanged(const QString &arg1);
    void on_lineEdit_token_textChanged(const QString &arg1);

    void on_radioButton_standard_toggled(bool checked);
    void on_radioButton_accelerated_toggled(bool checked);

    void on_action_Output_triggered();
};

#endif // APIDAEAPIWINDOW_H
