/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
#include "apidaeapiwindow.h"
#include "ui_apidaeapiwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QHttpPart>
#include <QMimeDatabase>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QStandardPaths>
#include <QDebug>
#include <QSettings>
#include <QDesktopServices>

// api interaction
void ApidaeAPIexample::on_action_Simulate_triggered()
{
    enableSimulationIfReady(); // one last check
    if (m_ready_for_simulation){
        disableInput();
        disableSimulation();
        ui->progressBar->show();
        socketSetupConnection(m_websocket_url);
    }
}

void ApidaeAPIexample::socketSetupConnection(const QUrl &url)
{
    connect(&m_webSocket, &QWebSocket::connected,
            this, &ApidaeAPIexample::onSocketConnected);
    connect(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &ApidaeAPIexample::onSocketTextReceived);
    m_webSocket.open(QUrl(url));
}

void ApidaeAPIexample::onSocketConnected()
{
    ui->statusBar->showMessage("Apidae API connected", 5000);
    m_webSocket.sendTextMessage("{\"event\": \"/uuid\"}");
}

void ApidaeAPIexample::onSocketTextReceived(QString message)
{
    qDebug("onTextMessageReceived");
    qDebug() << message;
    ui->textBrowser_console->insertPlainText(message + "\n");

    QJsonDocument json_response = QJsonDocument::fromJson(message.toLocal8Bit());
    QVariantMap message_items(json_response.object().toVariantMap());
    qDebug() << message_items.keys();
    if (message_items.keys().contains("uuid")) {
        qDebug("sending request");
        m_request  = new QNetworkRequest(QUrl(m_rest_url));
        QHttpPart emailPart;
        emailPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"email\""));
        emailPart.setBody(m_email.toLocal8Bit());
        qDebug() << m_email;
        m_files_post->append(emailPart);

        QHttpPart websocketPart;
        websocketPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"websocket\""));
        qDebug() << message_items.value("uuid").toString();
        websocketPart.setBody(message_items.value("uuid").toByteArray());
        m_files_post->append(websocketPart);

        QHttpPart tokenPart;
        tokenPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"token\""));
        tokenPart.setBody(m_apidae_api_token.toLocal8Bit());
        qDebug() << m_apidae_api_token;
        m_files_post->append(tokenPart);

        QFile idf(ui->lineEdit_idf->text());
        if (idf.open(QFile::ReadOnly | QFile::Text)){
            QHttpPart idfPart;
            idfPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/plaintext"));
            idfPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"idf\"; filename=\"" + idf.fileName() + "\""));
            idfPart.setBody(idf.readAll());
            m_files_post->append(idfPart);
        }
        QFile epw(ui->lineEdit_epw->text());
        if (epw.open(QFile::ReadOnly | QFile::Text)){
            QHttpPart epwPart;
            epwPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"epw\"; filename=\"" + epw.fileName() + "\""));
            epwPart.setBody(epw.readAll());
            m_files_post->append(epwPart);
        }

        if (!m_standard){
            QHttpPart standardPart;
            standardPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"run_standard\""));
            standardPart.setBody("false"); //run only the accelerated simulation
            m_files_post->append(standardPart);
        }

        if (!m_accelerated){
            QHttpPart fastPart;
            fastPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"run_fast\""));
            fastPart.setBody("false"); //run only the accelerated simulation
            m_files_post->append(fastPart);
        }

        rest_reply = manager.post(*m_request, m_files_post);
        m_files_post->setParent(rest_reply);

        connect(rest_reply, &QIODevice::readyRead,
                this, &ApidaeAPIexample::restReadyRead);
        connect(rest_reply, &QNetworkReply::uploadProgress,
                this, &ApidaeAPIexample::updateProgress);

        ui->statusBar->showMessage("Uploading input...");
    } else if (message_items.keys().contains("event") && message_items.value("event").toString().contains("/accelerator/study/complete")) { //get the zip file
        qDebug("getting results");

        m_accelerator_job_uuid = message_items.value("model_space_uuid").toString();
        //TODO: confirm status is "valid" before requesting download
        QNetworkRequest download_request(message_items.value("results_url").toString());
        download_reply = manager.get(download_request);

        connect(download_reply, &QNetworkReply::downloadProgress,
                this, &ApidaeAPIexample::updateProgress);
        connect(&manager, SIGNAL(finished(QNetworkReply*)),
                this, SLOT(downloadFinished(QNetworkReply*)));
        ui->progressBar->show();
        ui->statusBar->showMessage("Downloading output...");
    } else {
        ui->statusBar->showMessage("error");
    }
}

void ApidaeAPIexample::restReadyRead()
{
    qDebug("restReadyRead");
    QString reply_content(rest_reply->readAll());
    qDebug() << reply_content;
    ui->textBrowser_console->insertPlainText(reply_content + "\n");
    if (reply_content.contains("{\"error\":null,\"data\":{\"model_uuid\":")) {
        ui->statusBar->showMessage("Simulating...");
    } else {
        ui->statusBar->showMessage("Error.");
    }
}

void ApidaeAPIexample::downloadFinished(QNetworkReply* reply)
{
    qDebug("downloadFinished");
    QUrl url = reply->url();
    if (reply->error()) {
        fprintf(stderr, "Download of %s failed: %s\n",
                url.toEncoded().constData(),
                qPrintable(reply->errorString()));
    } else {
        QFileInfo fi(m_idf_file);
        QString prefix("/" + fi.completeBaseName());
        QString filename = QString(QStandardPaths::standardLocations(QStandardPaths::DownloadLocation).value(0)
                                   + prefix + "_%1.zip").arg(m_accelerator_job_uuid);
        if (saveToDisk(filename, reply)){
            printf("Download of %s succeeded (saved to %s)\n",
                   url.toEncoded().constData(), qPrintable(filename));
            ui->statusBar->showMessage("Downloaded: " + filename);
            ui->action_Output->setEnabled(true);
            m_output_filepath = filename;
        }
    }

    reply->deleteLater();
}

void ApidaeAPIexample::updateProgress(qint64 current, qint64 total)
{
    qint64 progress = 0;
    if (total > 0) {
        progress = (current*100)/total;
    }
    ui->progressBar->setValue(progress);
    if (progress == 100) {
        ui->progressBar->hide();
        ui->statusBar->showMessage("Simulating...");
    }
}

bool ApidaeAPIexample::saveToDisk(const QString &filename, QIODevice *data)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        fprintf(stderr, "Could not open %s for writing: %s\n",
                qPrintable(filename),
                qPrintable(file.errorString()));
        return false;
    }

    qDebug() << filename;


    file.write(data->readAll());
    file.close();

    return true;
}

// user interface
ApidaeAPIexample::ApidaeAPIexample(QString idf_file, QString epw_file, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ApidaeAPIwindow),
    m_idf_file(idf_file),
    m_epw_file(epw_file),
    m_files_post(new QHttpMultiPart(QHttpMultiPart::FormDataType)),
    m_ready_for_simulation(false)
{
    ui->setupUi(this);
    ui->progressBar->hide();
    loadSettings();
    enableSimulation();
    //  enableSimulationIfReady();
}

ApidaeAPIexample::~ApidaeAPIexample()
{
    saveSettings();
    delete ui;
}

void ApidaeAPIexample::on_action_IDF_triggered()
{
    m_idf_file =  QFileDialog::getOpenFileName(this
                                               ,"Select Model File"
                                               , QDir::homePath()
                                               , "IDF (*.idf)");
    ui->lineEdit_idf->setText(m_idf_file);
    enableSimulationIfReady();
}

void ApidaeAPIexample::on_action_EPW_triggered()
{
    m_epw_file = QFileDialog::getOpenFileName(this
                                              ,"Select Weather File"
                                              , QDir::homePath()
                                              , "EPW (*.epw)");
    ui->lineEdit_epw->setText(m_epw_file);
    enableSimulationIfReady();
}

void ApidaeAPIexample::on_action_Reference_triggered()
{
    QDesktopServices::openUrl(QUrl("http://apidaelabs.com/api", QUrl::TolerantMode));
}

void ApidaeAPIexample::disableSimulation()
{
    ui->pushButton_simulate->setEnabled(false);
    ui->action_Simulate->setEnabled(false);
}

void ApidaeAPIexample::disableInput()
{
    ui->action_IDF->setEnabled(false);
    ui->action_EPW->setEnabled(false);
    ui->lineEdit_email->setEnabled(false);
    ui->lineEdit_token->setEnabled(false);
    ui->radioButton_accelerated->setEnabled(false);
    ui->radioButton_standard->setEnabled(false);
}

void ApidaeAPIexample::enableSimulation()
{
    ui->pushButton_simulate->setEnabled(true);
    ui->action_Simulate->setEnabled(true);
}

void ApidaeAPIexample::enableSimulationIfReady()
{
    if (QFile::exists(m_idf_file)
            && QFile::exists(m_epw_file)
            && m_email.contains("@")
            && m_email.contains(".")
            && m_apidae_api_token.count() > 255
            && (m_accelerated || m_standard)) {
        m_ready_for_simulation = true;
        enableSimulation();
        saveSettings();
    } else {
        m_ready_for_simulation = false;
        disableSimulation();
    }
}

void ApidaeAPIexample::saveSettings()
{
    QSettings store(QSettings::IniFormat, QSettings::UserScope,
                    QCoreApplication::organizationName(),
                    QCoreApplication::applicationName());
    store.setValue("idf_file", m_idf_file);
    store.setValue("epw_file", m_epw_file);
    store.setValue("api_token", m_apidae_api_token);
    store.setValue("email", m_email);
    store.setValue("accelerated", m_accelerated);
    store.setValue("standard", m_standard);
    store.setValue("rest_url", m_rest_url);
    store.setValue("websocket_url", m_websocket_url);
}


void ApidaeAPIexample::loadSettings()
{
    QSettings stored(QSettings::IniFormat, QSettings::UserScope,
                     QCoreApplication::organizationName(),
                     QCoreApplication::applicationName());
    qDebug() << "SETTINGS: " << stored.fileName();
    m_rest_url = stored.value("rest_url", APIDAE_REST).toString();
    m_websocket_url = stored.value("websocket_url", APIDAE_SOCKET).toString();

    if (m_idf_file.isEmpty())
        m_idf_file = stored.value("idf_file").toString();
    ui->lineEdit_idf->setText(m_idf_file);

    if (m_epw_file.isEmpty())
        m_epw_file = stored.value("epw_file").toString();
    ui->lineEdit_epw->setText(m_epw_file);

    ui->lineEdit_token->setText(stored.value("api_token").toString());
    ui->lineEdit_email->setText(stored.value("email").toString());
    ui->radioButton_accelerated->setChecked(stored.value("accelerated", true).toBool());
    ui->radioButton_standard->setChecked(stored.value("standard", false).toBool());
}

void ApidaeAPIexample::on_action_Quit_triggered()
{
    close();
}


void ApidaeAPIexample::on_lineEdit_email_textChanged(const QString &arg1)
{
    m_email = arg1;
    enableSimulationIfReady();
}

void ApidaeAPIexample::on_lineEdit_token_textChanged(const QString &arg1)
{
    m_apidae_api_token = arg1;
    enableSimulationIfReady();
}


void ApidaeAPIexample::on_radioButton_standard_toggled(bool checked)
{
    m_standard = checked;
    enableSimulationIfReady();
}

void ApidaeAPIexample::on_radioButton_accelerated_toggled(bool checked)
{
    m_accelerated = checked;
    enableSimulationIfReady();
}

void ApidaeAPIexample::on_action_Output_triggered()
{
    QFileInfo fi(m_output_filepath);
    QDesktopServices::openUrl(QUrl(fi.canonicalPath(), QUrl::TolerantMode));
}
